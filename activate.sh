#!/bin/bash

if [ ! -d ".venv" ]; then
    virtualenv -p python3.6 .venv
fi


. ./.venv/bin/activate
