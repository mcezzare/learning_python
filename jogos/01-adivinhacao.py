"""
This module is an exercise from alura's course 1
"""
print("##################################")
print("Bem vindo ao jogo de Adivinhação!")
print("##################################")

numero_secreto = 42 # pylint: disable=invalid-name

chute_str = input("Digite um nº: ") # pylint: disable=invalid-name

print("Você digitou ", chute_str)

chute_int = int(chute_str) # pylint: disable=invalid-name
"""""
if (numero_secreto == chute_int):
    print("Acertou")
else:
    if (numero_secreto < chute_int):
        print("Ops foi maior :( ")
    elif (numero_secreto > chute_int):
        print("Ops foi menor :( ")
"""

acertou = numero_secreto == chute_int # pylint: disable=invalid-name
maior = chute_int > numero_secreto # pylint: disable=invalid-name
menor = chute_int < numero_secreto # pylint: disable=invalid-name

if acertou:
    print(":) Acertou ")

if maior:
    print("Ops foi maior :( ")
elif menor:
    print("Ops foi menor :( ")

print("Fim")
