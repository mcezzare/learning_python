"""
This module is an exercise from alura's course 1
"""
print("##################################")
print("Bem vindo ao jogo de Adivinhação!")
print("##################################")

numero_secreto = 42
total_de_tentativas = 3

for rodada in range(1,total_de_tentativas+1):
    #print("Tentativa " , rodada , " de " , total_de_tentativas)
    print("Tentativa {} de {}".format(rodada , total_de_tentativas))
    chute_str = input("Digite um número entre {} e {}: ".format(1,100))
    print("Você digitou: ", chute_str)
    chute = int(chute_str)

    if (chute < 1 or chute > 100):
        print("Entre 1 e 100 TRU *_*")
        continue

    acertou = numero_secreto == chute
    maior = chute > numero_secreto
    menor = chute < numero_secreto


    if (acertou):
        print("Você acertou!")
        break
    else:
        if (maior):
            print("Você errou! O seu chute foi maior que o número secreto.")
        elif (menor):
            print("Você errou! O seu chute foi menor que o número secreto.")


print("Fim do jogo")