"""
Formatando strings : https://docs.python.org/3/library/string.html#formatexamples
Aula 05 capitulo 09

"""

print("Tentativa {} de {}".format(3,10))

#valores invertidos
print("Tentativa {1} de {0}".format(10,3))

#FLoat
print("R$ {:f}".format(1.59))
print("R$ {:7}".format(1.59))

print("R$ {:7.2f}".format(1.59))

print("R$ {:7.2f}".format(1234.59))

print("R$ {:07.2f}".format(14.59))
print("R$ {:07.2f}".format(1234.59))

#arredonda:
print("R$ arredondado {:07.1f}".format(1234.59))

#Int

print("R$ {:07d}".format(14))
print("R$ {:03d}".format(14))
print("R$ {:02d}".format(154))

#outros
print("Data {:02d}/{:02d}".format(12,9))

print('{0}, {1}, {2}'.format('a', 'b', 'c'))
print('{0}, {2}, {1}'.format('a', 'b', 'c'))
print('{0}, {2}, {1}, {0}'.format('a', 'b', 'c'))
print('Coordinates: {latitude}, {longitude}'.format(latitude='37.24N', longitude='-115.81W'))
coord = {'latitude': '37.24N', 'longitude': '-115.81W'}
print('Coordinates: {latitude}, {longitude}'.format(**coord))