"""
i-9 inclusivos
10 - exclusivo
"""

def print_line():
    for i in range(1,10):
        print("-",' ',end='-')
    print()

print_line()
for i in range(1,10):
    print(i)

print_line()
print ("2º for")
for i in range(1,10,2):
    print(i)
